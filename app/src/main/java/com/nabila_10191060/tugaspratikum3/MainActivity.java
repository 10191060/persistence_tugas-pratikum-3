package com.nabila_10191060.tugaspratikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Deklarasi variable pendukung
    private TextView Hasil;
    private TextView Masukan;
    private TextView Eksekusi;

    //Deklarasi dan inisialisasi SharedPreferences
    private SharedPreferences preferences;

    //Digunakan untuk konfigurasi SharedPreferences
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Masukan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

        //Membuat file baru beserta modifernya
        preferences = getSharedPreferences("Belajar_SharedPreferences", Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getData(){
        //Mendapatkan input dari user
        String getKonten = Masukan.getText().toString();
        //Digunakan untuk pengaturan kofigurasi SharedPreferences
        editor = preferences.edit();
        //Memasukkan data pada editor SharePreferences dengan key (data_saya)
        editor.putString("data_saya", getKonten);
        //Menajalankan Operasi
        editor.apply();
        //Menampilkan output
        Hasil.setText("Output Data :"+preferences.getString("data_saya", null));
    }
}